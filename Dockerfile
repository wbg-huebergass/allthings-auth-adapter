FROM crystallang/crystal:1.14.0-alpine
MAINTAINER daniel@illi.zone

ADD . /app
WORKDIR /app
RUN shards install && shards build --production --static --release

FROM scratch
COPY --from=0 /app/bin/allthings-auth-adapter /allthings-auth-adapter

EXPOSE 3000

ENTRYPOINT ["/allthings-auth-adapter"]
