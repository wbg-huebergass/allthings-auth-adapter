require "../spec_helper"
require "../../src/profile_adapters/nextcloud"

describe ProfileAdapters::Nextcloud do
  context "to_h" do
    it "is correct for huebergass profile" do
      ProfileAdapters::Nextcloud.new(huebergass_profile).to_h.should eq({
        "id"          => "5eb56c04500b7200071472a2",
        "sub"         => "5eb56c04500b7200071472a2",
        "email"       => "testuser@example.com",
        "name"        => "Test User",
        "displayName" => "Test User",
        "picture"     => "https://cloud.allthings.me/default/f1f54761cd957951d81dfb8ff5c39058_5eb5a680c4e82_a857ab906ac68e03438142180bb5eebd_o.jpg",
        "groups"      => ["huebergass"],
      })
    end

    it "is correct for non-huebergass profile" do
      ProfileAdapters::Nextcloud.new(non_huebergass_profile).to_h.should eq({
        "id"          => "5eb56c04500b7200071472a2",
        "sub"         => "5eb56c04500b7200071472a2",
        "email"       => "testuser@example.com",
        "name"        => "Test User",
        "displayName" => "Test User",
        "picture"     => "https://cloud.allthings.me/default/f1f54761cd957951d81dfb8ff5c39058_5eb5a680c4e82_a857ab906ac68e03438142180bb5eebd_o.jpg",
        "groups"      => [] of String,
      })
    end
  end
end
