require "./spec_helper"
require "../src/app"
require "xml"

describe App do
  # You can use get,post,put,patch,delete to call the corresponding route.
  it "renders /" do
    get "/"
    response.body.should eq "Hello World!"
  end

  context "/authorize" do
    it "redirects to allthings" do
      get "/authorize"

      response.status.should eq HTTP::Status::FOUND
      response.body.should be_empty
      response.headers["Location"].should be_a String

      allthings_uri = URI.parse(App::ALLTHINGS_AUTHORIZE_URL)

      redirect_uri = URI.parse(response.headers["Location"])
      redirect_uri.scheme.should eq allthings_uri.scheme
      redirect_uri.host.should eq allthings_uri.host
      redirect_uri.path.should eq allthings_uri.path
    end

    it "replaces scope param" do
      get "/authorize?scope=openid"

      redirect_uri = URI.parse(response.headers["Location"])
      redirect_params = redirect_uri.query_params

      redirect_params.should eq HTTP::Params.parse("scope=user-profile:read")
    end

    it "adds scope param if missing" do
      get "/authorize"

      redirect_uri = URI.parse(response.headers["Location"])
      redirect_params = redirect_uri.query_params

      redirect_params.should eq HTTP::Params.parse("scope=user-profile:read")
    end

    it "does not change params other than scope" do
      get "/authorize?hello=world"

      redirect_uri = URI.parse(response.headers["Location"])
      redirect_params = redirect_uri.query_params

      redirect_params.should eq HTTP::Params.parse("scope=user-profile:read&hello=world")
    end
  end

  context "/profile/nextcloud" do
    it "transforms response" do
      WebMock
        .stub(:get, "https://api.allthings.me/api/v1/me")
        .with(headers: {"Authorization" => "test-token"})
        .to_return(body: File.read("spec/fixtures/files/huebergass-profile.json"))

      get "/profile/nextcloud", headers: HTTP::Headers{"Authorization" => "test-token"}

      response.status.should eq HTTP::Status::OK

      response_json = JSON.parse(response.body).as_h
      response_json.should eq({
        "id"          => "5eb56c04500b7200071472a2",
        "sub"         => "5eb56c04500b7200071472a2",
        "email"       => "testuser@example.com",
        "name"        => "Test User", # used by 'oidc-login'
        "displayName" => "Test User", # used by 'social login'
        "picture"     => "https://cloud.allthings.me/default/f1f54761cd957951d81dfb8ff5c39058_5eb5a680c4e82_a857ab906ac68e03438142180bb5eebd_o.jpg",
        "groups"      => ["huebergass"],
      })
    end

    it "returns error" do
      WebMock
        .stub(:get, "https://api.allthings.me/api/v1/me")
        .with(headers: {"Authorization" => "test-token"})
        .to_return(status: 500, body: "an error occurred")

      get "/profile/nextcloud", headers: HTTP::Headers{"Authorization" => "test-token"}

      response.status.should eq HTTP::Status::INTERNAL_SERVER_ERROR
      response.body.should eq "an error occurred"
    end
  end

  context "/userinfo" do
    it "transforms response" do
      WebMock
        .stub(:get, "https://api.allthings.me/api/v1/me")
        .with(headers: {"Authorization" => "test-token"})
        .to_return(body: File.read("spec/fixtures/files/huebergass-profile.json"))

      get "/userinfo", headers: HTTP::Headers{"Authorization" => "test-token"}

      response.status.should eq HTTP::Status::OK

      response_json = JSON.parse(response.body).as_h
      response_json.should eq({
        "sub"     => "5eb56c04500b7200071472a2",
        "name"    => "Test User",
        "picture" => "https://cloud.allthings.me/default/f1f54761cd957951d81dfb8ff5c39058_5eb5a680c4e82_a857ab906ac68e03438142180bb5eebd_o.jpg",
        "email"   => "testuser@example.com",
        "roles"   => ["huebergass"],
      })
    end

    it "returns error" do
      WebMock
        .stub(:get, "https://api.allthings.me/api/v1/me")
        .with(headers: {"Authorization" => "test-token"})
        .to_return(status: 500, body: "an error occurred")

      get "/userinfo", headers: HTTP::Headers{"Authorization" => "test-token"}

      response.status.should eq HTTP::Status::INTERNAL_SERVER_ERROR
      response.body.should eq "an error occurred"
    end
  end
end
