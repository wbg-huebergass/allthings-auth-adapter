ENV["KEMAL_ENV"] = "test"

require "spec-kemal"
require "webmock"
require "../src/allthings/profile"

Spec.before_each &->WebMock.reset

def huebergass_profile
  profile_text = File.read("spec/fixtures/files/huebergass-profile.json")
  Allthings::Profile.new(profile_text)
end

def non_huebergass_profile
  profile_text = File.read("spec/fixtures/files/non-huebergass-profile.json")
  Allthings::Profile.new(profile_text)
end

def no_utilisation_period_profile
  profile_text = File.read("spec/fixtures/files/no-utilisation-period-profile.json")
  Allthings::Profile.new(profile_text)
end
