require "../spec_helper"
require "../../src/allthings/profile"

describe Allthings::Profile do
  it "has the id" do
    huebergass_profile.id.should eq "5eb56c04500b7200071472a2"
  end

  it "has the full_name" do
    huebergass_profile.full_name.should eq "Test User"
  end

  it "has the email" do
    huebergass_profile.email.should eq "testuser@example.com"
  end

  it "has the image_url" do
    huebergass_profile.image_url.should eq "https://cloud.allthings.me/default/f1f54761cd957951d81dfb8ff5c39058_5eb5a680c4e82_a857ab906ac68e03438142180bb5eebd_o.jpg"
  end

  context "with huebergass profile" do
    it "has huebergass role" do
      huebergass_profile.roles.should eq ["huebergass"]
    end

    it "huebergass? is true" do
      huebergass_profile.huebergass?.should eq true
    end
  end

  context "with non-huebergass profile" do
    it "does not have huebergass role" do
      non_huebergass_profile.roles.should eq [] of String
    end

    it "huebergass? is false" do
      non_huebergass_profile.huebergass?.should eq false
    end
  end

  context "with no-utilisation-period profile" do
    it "does not have huebergass role" do
      no_utilisation_period_profile.roles.should eq [] of String
    end

    it "huebergass? is false" do
      no_utilisation_period_profile.huebergass?.should eq false
    end
  end
end
