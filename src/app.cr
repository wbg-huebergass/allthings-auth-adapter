require "kemal"
require "./allthings"
require "./allthings/profile"
require "./profile_adapters/nextcloud"
require "./profile_adapters/oidc_userinfo"

module App
  VERSION = "0.1.5"

  ALLTHINGS_AUTHORIZE_URL = "https://accounts.allthings.me/oauth/authorize"
  ALLTHINGS_PROFILE_URL   = "https://api.allthings.me/api/v1/me"

  get "/" do
    "Hello World!"
  end

  get "/authorize" do |env|
    params = env.params.query.dup

    # let's re-write the scope parameter for compatibility with allthings.
    # "user-profile:read" must be the single scope value.
    params["scope"] = "user-profile:read"

    # redirect the client to the original allthings authorize endpoint with the patched params
    redirect_url = "#{ALLTHINGS_AUTHORIZE_URL}?#{params}"
    log "redirecting to #{redirect_url.inspect}"
    env.redirect(redirect_url)
  end

  get "/profile/nextcloud" do |env|
    env.response.content_type = "application/json"

    with_profile(env) do |allthings_profile|
      # save_original_data(profile_attrs["id"], allthings_response.body)
      ProfileAdapters::Nextcloud.new(allthings_profile).to_json
    end
  end

  # return userinfo as specified in https://openid.net/specs/openid-connect-core-1_0.html#UserInfo
  get "/userinfo" do |env|
    env.response.content_type = "application/json"

    with_profile(env) do |allthings_profile|
      # save_original_data(profile_attrs["id"], allthings_response.body)
      ProfileAdapters::OidcUserinfo.new(allthings_profile).to_json
    end
  end

  get "/.well-known/openid-configuration" do |env|
    base_url = "https://#{env.request.headers["Host"]?}"

    {
      "issuer"                   => base_url,
      "authorization_endpoint"   => "#{base_url}/authorize",
      "token_endpoint"           => "https://accounts.allthings.me/oauth/token",
      "userinfo_endpoint"        => "#{base_url}/userinfo",
      "scopes_supported"         => ["openid"],
      "response_types_supported" => [
        # "code",
        # "code id_token",
        "code token",
        # "code id_token token",
        # "token",
        # "id_token",
        # "id_token token",
      ],
      "grant_types_supported" => [
        "authorization_code",
      ],
      "token_endpoint_auth_methods_supported" => ["client_secret_post"],
      "claims_parameter_supported"            => false,
      "request_parameter_supported"           => false,
      "request_uri_parameter_supported"       => false,
    }.to_json
  end

  def self.with_profile(env)
    allthings_response = query_profile(env)

    if allthings_response.status_code == 200
      yield Allthings::Profile.new(allthings_response.body)
    else
      handle_error(env, allthings_response)
    end
  end

  def self.query_profile(env)
    auth_header = env.request.headers["Authorization"]

    allthings_uri = URI.parse("https://api.allthings.me/api/v1/me")

    # disable server cert verification as it seems to be broken in the docker container # TODO fix
    context = OpenSSL::SSL::Context::Client.insecure
    client = HTTP::Client.new(allthings_uri, tls: context)

    client.tls?

    client.before_request do |request|
      request.headers["Authorization"] = auth_header
    end

    allthings_response = client.get(allthings_uri.path)
  end

  def self.handle_error(env, allthings_response)
    log "received error from #{ALLTHINGS_PROFILE_URL}"
    log "status: #{allthings_response.status_code}"

    env.response.status_code = allthings_response.status_code
    env.response.content_type = allthings_response.headers["Content-Type"] if allthings_response.headers.has_key?("Content-Type")

    allthings_response.body
  end

  def self.save_original_data(id, original_data)
    filename = "#{id.to_s}.json"
    File.write(filename, original_data)
  end

  Kemal.run
end
