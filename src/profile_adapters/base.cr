require "json"

module ProfileAdapters
  class Base
    getter :profile

    def initialize(profile : Allthings::Profile)
      @profile = profile
    end

    def to_json
      to_h.to_json
    end
  end
end
