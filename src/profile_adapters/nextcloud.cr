require "./base"

module ProfileAdapters
  class Nextcloud < Base
    def to_h
      {
        "id"          => profile.id,
        "sub"         => profile.id,
        "email"       => profile.email,
        "name"        => profile.full_name, # used by oidc-login
        "displayName" => profile.full_name, # used by social-login
        "picture"     => profile.image_url,
        "groups"      => profile.roles,
      }
    end
  end
end
