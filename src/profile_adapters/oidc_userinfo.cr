require "./base"

module ProfileAdapters
  class OidcUserinfo < Base
    def to_h
      {
        "sub"     => profile.id,
        "email"   => profile.email,
        "name"    => profile.full_name,
        "picture" => profile.image_url,
        "roles"   => profile.roles,
      }
    end
  end
end
