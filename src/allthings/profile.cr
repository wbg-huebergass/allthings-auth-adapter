require "../allthings"

module Allthings
  class Profile
    @attributes : Hash(String, JSON::Any)

    def initialize(json : String)
      @json = json
      @attributes = JSON.parse(@json).as_h
    end

    def id
      @attributes["id"]
    end

    def full_name
      @attributes["username"]
    end

    def email
      @attributes["email"]
    end

    def image_url
      @attributes.dig?("_embedded", "profileImage", "files", "original", "url")
    end

    def roles
      huebergass? ? ["huebergass"] : [] of String
    end

    def huebergass?
      @attributes.dig("_embedded", "utilisationPeriods").as_a.any? do |utilisation_period|
        utilisation_period.dig("_embedded", "property", "id") == HUEBERGASS_PROPERTY_ID
      end
    rescue KeyError
      false
    end
  end
end
